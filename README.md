# EBS Resize

## Overview

A background service that monitors free disk space on an EC2 instance,
and automatically increases the volume size when the free space is
below a certain threshold.

## Usage

1. Create IAM user that has the following permissions:
    - DescribeInstances
    - DescribeVolumes
    - ModifyVolume
2. Configure AWS API on the target EC2 instance using `aws configure`
(see [Boto Configuration](http://boto3.readthedocs.io/en/latest/guide/configuration.html)).
3. Run `ebsresize` as a local user allowed to execute `sudo resize2fs`.

## Default Configuration

 - Processing interval: 60s
 - Volume size threshold: <30% of free disk space
 - Volume size increase multiplier: 2x
 - Excluded block devices: /dev/sda, /dev/sda1, /dev/xvda, /dev/xvda1
 