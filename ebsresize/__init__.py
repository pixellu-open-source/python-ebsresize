import sys
import os
import os.path
import math
import time
import subprocess
import shlex
import re
import logging
from pprint import pformat

import requests
import boto3
from apscheduler.schedulers.blocking import BlockingScheduler


_DEFAULT_PROCESSING_INTERVAL = 60


class EC2InstanceIDError(Exception):
    pass


class EBSVolumesResize(object):

    _AWS_API_URL_INSTANCE_ID = "http://169.254.169.254/latest/meta-data/instance-id"

    _KEY_BLOCK_SIZE = 'block_size'
    _KEY_BLOCK_COUNT = 'block_count'
    _KEY_FREE_BLOCKS = 'free_blocks'
    _KEY_FREE_GB = 'free_gb'
    _KEY_FREE_PERCENTAGE = 'free_percentage'

    _MAX_RETRIEVE_ATTEMPTS = 10
    _RETRIEVE_DELAY_SEC = 5

    _DEFAULT_STATS_PROGRAM = 'df'  # Possible values: df, tune2fs
    _DEFAULT_EXCLUDED_LOCAL_VOLUMES = ["/dev/sda", "/dev/sda1", "/dev/xvda", "/dev/xvda1"]  # Do not process root volume
    _DEFAULT_THRESHOLD_FREE_SPACE_PERCENTAGE = 0.3
    _DEFAULT_INCREMENT_MULTIPLIER = 2.0
    _DEFAULT_INCREMENT_ADDEND_GB = 0.0

    def __init__(self):
        self._instance_id = None
        self._ec2 = None
        self._volumes = {}  # Volumes' info as retrieved from ec2.describe_instances()
        self._volumes_desc = {}  # Volumes' info as retrieved from ec2.describe_volumes()
        self._local_volume_mapping = {}
        self._local_volume_stats = {}

        self._stats_program = None
        self._excluded_local_volumes = None
        self._threshold_free_space_percentage = None
        self._increment_multiplier = None
        self._increment_addend_gb = None
        self.config()

        self.connect()

    def config(self, **kwargs):
        """
        Configures the instance. Uses default values, if a kwarg is not provided.
        :param stats_program: The program used to retrieve the local FS disk space info
        :param excluded_local_volumes: The list of block devices to exclude when evaluating for a possible size increase
        :param threshold_free_space_percentage: Increase the volume size, if the available disk space is less than the 
                                                threshold.
        :param increment_multiplier: When a volume size is increased, its size is multiplied by this factor.                                                
        :param increment_addend_gb: When a volume size is increased, its size is increased by this parameter.                                                
        :return: None
        """
        self._stats_program = kwargs.get('stats_program', self._DEFAULT_STATS_PROGRAM)
        self._excluded_local_volumes = kwargs.get('excluded_local_volumes', self._DEFAULT_EXCLUDED_LOCAL_VOLUMES)
        self._threshold_free_space_percentage = kwargs.get('threshold_free_space_percentage', self._DEFAULT_THRESHOLD_FREE_SPACE_PERCENTAGE)
        self._increment_multiplier = kwargs.get('increment_multiplier', self._DEFAULT_INCREMENT_MULTIPLIER)
        self._increment_addend_gb = kwargs.get('increment_addend_gb', self._DEFAULT_INCREMENT_ADDEND_GB)

    def connect(self):
        """
        Retrieves the current instance ID, and establishes the connection with EC2 API.
        :return: None
        """
        self._instance_id = self.get_current_instance_id()
        if self._instance_id is None:
            raise EC2InstanceIDError("Instance ID could not be determined")
        self._ec2 = boto3.client('ec2')

    def process_size_increase(self):
        """
        Goes over each local volume checking for the available space. If the amount of free disk space is lower than
        the threshold value, then increase the volume size according to the configured parameters.
        :return: None
        """

        # Update all of the volume before running evaluations:
        self._retrieve_instance_volumes()
        self._map_local_volumes()
        self._retrieve_fs_stats()

        for local_volume in sorted(self._local_volume_mapping):
            # Skip volumes from the predefined list:
            if local_volume in self._excluded_local_volumes:
                continue

            if local_volume in self._local_volume_stats:
                volume_id = self._volume_id_for_local_volume(local_volume)
                stats = self._local_volume_stats[local_volume]
                if stats[self._KEY_FREE_PERCENTAGE] <= self._threshold_free_space_percentage:
                    # Calculate the new size:
                    new_size = float(self._volumes_desc[volume_id]['Size'])\
                               * float(self._increment_multiplier)\
                               + float(self._increment_addend_gb)
                    new_size = int(math.ceil(new_size))
                    try:
                        self.resize_volume(local_volume, new_size)
                    except:
                        logging.error("Error occurred while resizing volume %s. Exception: %s"
                                      % (local_volume, str(sys.exc_info())))

            else:
                logging.warning("Stats for volume %s not found!" % local_volume)

    def resize_volume(self, local_volume, new_size):
        """
        Resizes the volume to the new size. Volumes can only be expanded, not shrunk.
        :param local_volume: block device path of the local volume
        :param new_size: new size in GB  
        :return: True if resize succeeded; False otherwise
        """
        assert self._ec2 is not None

        volume_id = self._volume_id_for_local_volume(local_volume)
        assert volume_id is not None

        logging.info("Resizing the volume %s (%s) to new size %d GB ..." % (local_volume, volume_id, new_size))

        self._retrieve_volumes_description()

        if self._volumes_desc[volume_id]['Size'] == new_size:
            logging.error("Size of volume %s (%s) cannot be modified -- already at %d GB" % (local_volume, volume_id, new_size))
            return False
        if self._volumes_desc[volume_id]['Size'] > new_size:
            logging.error("Size of volume %s (%s) cannot be modified -- shrinking not allowed" % (local_volume, volume_id))
            return False

        try:
            self._ec2.modify_volume(VolumeId=volume_id, Size=new_size)
        except:
            logging.error("Volume modification request failed for volume %s (%s). Exception: %s"
                          % (local_volume, volume_id, str(sys.exc_info())))
            return False

        attempts = 0

        while True:

            time.sleep(self._RETRIEVE_DELAY_SEC)

            self._retrieve_volumes_description()
            if self._volumes_desc[volume_id]['State'] == 'in-use' and self._volumes_desc[volume_id]['Size'] == new_size:
                break

            attempts += 1

            # Fail after a certain number of attempts:
            if attempts >= self._MAX_RETRIEVE_ATTEMPTS:
                logging.error("Max number of attempts to retrieve the volume info for %s (%s) has been reached."
                              % (local_volume, volume_id))
                return False

        try:
            output = subprocess.check_output("sudo resize2fs %s" % (shlex.quote(local_volume)), shell=True)
        except:
            logging.error("Filesystem resize process failed. Exception: %s" % str(sys.exc_info()))
            return False

        logging.info("Volume %s (%s) has been resized to %d GB successfully!" % (local_volume, volume_id, new_size))

        return True

    @classmethod
    def get_current_instance_id(cls):
        """
        Gets the current EC2 instance ID using AWS API.
        :return: instance ID
        """
        try:
            ret = requests.get(cls._AWS_API_URL_INSTANCE_ID)
            ret = ret.text.strip()
        except:
            logging.error("Could not retrieve EC2 instance ID.")
            ret = None
        return ret

    @classmethod
    def _translate_block_device_path(cls, block_device_path):
        """
        Translates from /dev/sdx to /dev/xvdx.
        :param block_device_path: Full path to the block device (i.e. /dev/sda) 
        :return: Translated block device path
        """
        volume_name = os.path.basename(block_device_path)
        if volume_name.startswith("sd"):
            volume_name = "xvd" + volume_name[2:]
            return os.path.dirname(block_device_path) + "/" + volume_name
        elif volume_name.startswith("xvd"):
            return block_device_path
        else:
            return None

    def _retrieve_instance_volumes(self):
        """
        Retrieves volumes attached to the current instance using AWS API.
        :return: None 
        """
        desc = self._ec2.describe_instances(InstanceIds=[self._instance_id])
        for reservation in desc['Reservations']:
            for instance in reservation['Instances']:
                if instance['InstanceId'] == self._instance_id:
                    for block_device in instance['BlockDeviceMappings']:
                        if 'Ebs' in block_device:
                            self._volumes[block_device['DeviceName']] = block_device['Ebs']
        self._retrieve_volumes_description()

    def _retrieve_volumes_description(self):
        """
        Retrieves descriptions of EBS volumes using AWS API.
        :return: None 
        """
        volume_ids = [volume['VolumeId'] for volume in self._volumes.values()]
        self._volumes_desc = self._ec2.describe_volumes(VolumeIds=volume_ids)
        self._volumes_desc = {volume['VolumeId']: volume for volume in self._volumes_desc['Volumes']}
        logging.debug("Volumes description: %s" % pformat(self._volumes_desc))

    def _map_local_volumes(self):
        """
        Maps local block device paths to the paths specified in the volume info.
        :return: 
        """
        for volume, desc in self._volumes.items():
            if os.path.isfile(volume):
                self._local_volume_mapping[volume] = volume
            else:
                translated_volume = self._translate_block_device_path(volume)
                self._local_volume_mapping[translated_volume] = volume
        logging.debug("Local volume mapping: %s " % pformat(self._local_volume_mapping))

    def _volume_id_for_local_volume(self, local_volume):

        for volume, desc in self._volumes.items():
            if volume == self._local_volume_mapping[local_volume]:
                return desc['VolumeId']
        return None

    def _retrieve_fs_stats(self):
        """
        Retrieves amount of free disk space for local volumes.
        :return: 
        """
        for local_volume in sorted(self._local_volume_mapping):
            try:
                if self._stats_program == "tune2fs":
                    output = subprocess.check_output(
                        "sudo tune2fs -l %s | grep 'Block size:\|Block count:\|Free blocks:'"
                        % (shlex.quote(local_volume)),
                        shell=True,
                    )
                    output = output.decode("utf-8")
                    block_size = int(re.findall(r"Block size:\s+(\d+)\b", output)[0])
                    block_count = int(re.findall(r"Block count:\s+(\d+)\b", output)[0])
                    free_blocks = int(re.findall(r"Free blocks:\s+(\d+)\b", output)[0])
                    free_percentage = float(free_blocks) / float(block_count)
                    free_gb = float(block_size / 1024) * float(free_blocks) / 1024.0 / 1024.0
                elif self._stats_program == "df":
                    output = subprocess.check_output(
                        "df %s"
                        % (shlex.quote(local_volume)),
                        shell=True,
                    )
                    output = output.decode("utf-8")
                    block_size = 1024
                    block_count, blocks_used, free_blocks = re.findall(
                        r"%s\s+(\d+)\s+(\d+)\s+(\d+)" % (local_volume),
                        output
                    )[0]
                    block_count, blocks_used, free_blocks = int(block_count), int(blocks_used), int(free_blocks)
                    free_percentage = float(free_blocks) / float(block_count)
                    free_gb = float(block_size / 1024) * float(free_blocks) / 1024.0 / 1024.0

                logging.info(
                    "Block device: %s, volume ID: %s, block size: %d, block count: %d, free blocks: %d, free GB: %.2f, free %%: %.2f,"
                    % (
                        local_volume,
                        self._volume_id_for_local_volume(local_volume),
                        block_size,
                        block_count,
                        free_blocks,
                        free_gb,
                        free_percentage)
                )

                self._local_volume_stats[local_volume] = {
                    self._KEY_BLOCK_SIZE: block_size,
                    self._KEY_BLOCK_COUNT: block_count,
                    self._KEY_FREE_BLOCKS: free_blocks,
                    self._KEY_FREE_GB: free_gb,
                    self._KEY_FREE_PERCENTAGE: free_percentage,
                }
            except:
                e = sys.exc_info()
                logging.error("Could not retrieve free space for volume %s. Exception: %s " % (local_volume, str(e)))


def main():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s: %(message)s")

    # TODO: Add CLI arguments parsing and runtime configuration
    volumes = EBSVolumesResize()
    volumes.connect()

    scheduler = BlockingScheduler()
    scheduler.add_job(volumes.process_size_increase, 'interval', seconds=_DEFAULT_PROCESSING_INTERVAL)

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass


if __name__ == '__main__':
    main()
