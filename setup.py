from distutils.core import setup

setup(
    name='ebsresize',
    version='0.1.0',
    packages=['ebsresize'],
    url='',
    license='BSD',
    author='Alex Prykhodko',
    author_email='alex@prykhodko.net',
    description='',
    entry_points={
        'console_scripts': [
            'ebsresize = ebsresize:main'
        ]
    },
    install_requires=[
        "boto3>=1.4.4",
        "requests>=2.9.1",
        "APScheduler>=3.3.1"
    ]
)
